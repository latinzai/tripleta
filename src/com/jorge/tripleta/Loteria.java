package com.jorge.tripleta;

import java.util.ArrayList;
import java.util.Date;

import com.jorge.tripleta.observer.LoteriaObserver;
import com.jorge.tripleta.observer.Observable;

public class Loteria implements Observable{
	
	private Date fecha;
	private ArrayList<LoteriaObserver> jugadores = new ArrayList<LoteriaObserver>();
	private int[] numerosGanadores;
	
	public void sacarTripleta(int numero1, int numero2, int numero3) {
		int[] ganadores = { numero1, numero2, numero3 };
		this.numerosGanadores = ganadores;
		notifyObserver();
	}

	@Override
	public void addObserver(LoteriaObserver o) {
		jugadores.add(o);
	}

	@Override
	public void removeObserver(LoteriaObserver o) {
		jugadores.remove(o);
	}

	@Override
	public void notifyObserver() {
		for (LoteriaObserver o : jugadores) {
			o.update(this.numerosGanadores);
		}
	}
	
		
}
