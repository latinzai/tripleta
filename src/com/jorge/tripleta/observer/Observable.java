package com.jorge.tripleta.observer;

public interface Observable {

	public void addObserver(LoteriaObserver o);
	
	public void removeObserver(LoteriaObserver o);
	
	public void notifyObserver();
}
