package com.jorge.tripleta.observer;

public interface LoteriaObserver {

	public void update(int[] ganadores);
		
}
