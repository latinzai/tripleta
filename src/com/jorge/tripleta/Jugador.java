package com.jorge.tripleta;

import java.util.ArrayList;
import java.util.Arrays;

import com.jorge.tripleta.observer.LoteriaObserver;

public class Jugador implements LoteriaObserver {

	private String nombre;
	private String apellido;
	private ArrayList<Juego> juegos;
	private Loteria loteria;
	
	public Jugador(Loteria loteria,String nombre, String apellido) {
		setNombre(nombre);
		setApellido(apellido);
		this.loteria = loteria;
		this.loteria.addObserver(this);
		juegos = new ArrayList<Juego>();
	}
	
	public void jugar(int numero1, int numero2, int numero3) {
		juegos.add(
				new Juego(
						new int[]{ numero1, numero2, numero3 }, 
						this
						));
		
	
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public void update(int[] ganadores) {
		
		for (Juego juego : juegos) {
			if (Arrays.equals(juego.getNumeros(), ganadores))
				System.out.println(getNombre() + " " + getApellido() + ": ME LO SAQUEEEEE!!");
		}
		
	}



	
	
	
}
