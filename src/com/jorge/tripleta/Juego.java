package com.jorge.tripleta;

import java.util.Date;
 
public class Juego {
	
	private int[] numeros;
	private Jugador jugador;
	private Date fecha;
	
	
	
	public Juego(int[] numeros, Jugador jugador) {
		setNumeros(numeros);
		setJugador(jugador);
		setFecha(new Date());
	}
	public int[] getNumeros() {
		return numeros;
	}
	public void setNumeros(int[] numeros) {
		this.numeros = numeros;
	}
	public Jugador getJugador() {
		return jugador;
	}
	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}
	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
	
	
	
}
